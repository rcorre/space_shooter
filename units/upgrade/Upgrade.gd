extends KinematicBody
class_name Upgrade

export(Resource) var weapon: Resource

onready var sprite: Sprite3D = $Sprite3D

func _ready():
	sprite.texture = weapon.icon

func set_held(held: bool):
	$Area.monitoring = held

func _on_Area_body_entered(body: Node):
	var other := body as Upgrade
	assert(other)
	match {weapon.name: true, other.weapon.name: true}:
		{"AUTO", "BEAM"}:
			weapon = load("res://items/weapons/autobeam/data.tres")
		{"AUTO", "FLAK"}:
			weapon = load("res://items/weapons/autoflak/data.tres")
		{"BEAM", "FLAK"}:
			weapon = load("res://items/weapons/flakbeam/data.tres")
		_:
			return
	sprite.texture = weapon.icon
	other.queue_free()
