extends Building

const POD_SCENE := preload("res://units/drop_pod/pod_small.tscn")

export(PackedScene) var unit_scene: PackedScene
export(int, 0, 100) var spawn_interval := 10

onready var spawn_points: Array = $SpawnPoints.get_children()

var timer: Timer

func build():
	.build()
	timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "spawn")
	timer.start(spawn_interval)
	connect("destroyed", self, "_on_destroyed")

func _on_destroyed():
	anim_player.play_backwards("Build")
	propagate_call("set_team", ["neutral"])

func spawn():
	for p in spawn_points:
		var pod := POD_SCENE.instance()
		pod.unit_scene = unit_scene
		get_tree().root.add_child(pod)
		pod.global_transform = p.global_transform
		pod.propagate_call("set_team", [team])

func _on_WeaponArea_grabbed(w: Weapon):
	connect("destroyed", w, "queue_free")
	build()
	propagate_call("set_team", [w.team])
