extends Spatial
class_name Entity

signal destroyed
signal damaged

export(float, 0, 1000) var MAX_HEALTH := 100.0
export(String, "", "red", "blue") var initial_team := ""

onready var health := MAX_HEALTH
var team := "neutral"

func _ready():
	if initial_team:
		propagate_call("set_team", [initial_team])

func set_team(val: String):
	if is_in_group(team):
		remove_from_group(team)
	team = val
	add_to_group(val)

func damage(amount: float):
	if health <= 0:
		return
	health -= amount
	emit_signal("damaged")
	if health <= 0:
		emit_signal("destroyed")
