extends Spatial

func _ready():
	var players := get_tree().get_nodes_in_group("player")
	if players:
		players[0].set_active(true)
	else:
		get_tree().get_nodes_in_group("spawn_point")[0].set_active(true)
