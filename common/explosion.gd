extends Node
class_name Explosion

const EXPLOSION_SCENE = preload("res://common/explosion.tscn")

static func create(parent: Node, pos: Vector3, scale: Vector3, team: String):
	var explosion = EXPLOSION_SCENE.instance()
	parent.add_child(explosion)
	explosion.scale = scale
	explosion.propagate_call("set_team", [team])
	explosion.global_transform.origin = pos
	var size := scale.length()
	explosion.get_node("AnimationPlayer").playback_speed = 1 / sqrt(size)
	explosion.get_node("Sound").unit_db += 2.5 * size
	explosion.get_node("Sound").unit_size += 2.5 * size
	return explosion
