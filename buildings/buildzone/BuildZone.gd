extends Area
class_name BuildZone

export(PackedScene) var building_scene: PackedScene

onready var anim_player: AnimationPlayer = $AnimationPlayer
onready var building_point: RemoteTransform = $BuildingPoint

var building: Building

func _ready():
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body):
	body.queue_free()
	if not building:
		build()

func build():
	building = building_scene.instance()
	get_tree().current_scene.add_child(building)
	building.propagate_call("set_team", ["blue"])
	building_point.remote_path = building.get_path()
	anim_player.play("Build")
	yield(anim_player, "animation_finished")
	building.build()
	yield(building, "destroyed")
	anim_player.play("Blueprint")
	building = null
