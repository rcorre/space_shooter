extends MarginContainer

export(float, EXP, 0, 1000) var max_dist := 400.0

var player: Spatial

func _process(delta: float):
	update()

func _draw():
	if not player:
		return

	var zoom := min(rect_size.x, rect_size.y) / max_dist / 2

	for n in get_tree().get_nodes_in_group(Global.opposing_team(player.team)):
		render(n, zoom, Color(1, 0, 0))
	for n in get_tree().get_nodes_in_group(player.team):
		render(n, zoom, Color(0, 0, 1))

func render(n: Node, zoom: float, c: Color):
		var disp := player.to_local((n as Spatial).global_transform.origin)
		var disp2 := Vector2(disp.x, disp.z)
		if disp2.length() < max_dist:
			draw_circle(rect_size / 2 - disp2 * zoom, 2, c)
