extends KinematicBody

const CAMERA_ZOOM_FACTOR := 20.0
const CAMERA_MOVE_FACTOR := 500.0
const MAX_ROLL := PI / 4
const ROLL_SPEED := 2.0
const MOUSE_SENSITIVITY := Vector2(0.05, 0.1)

export(float) var base_speed := 0.75
export(float) var max_speed := 1.5
export(float) var pitch_factor := 2.0
export(float) var yaw_factor := 2.0
export(float) var acceleration := 0.5
export(float) var max_hull := 20.0

onready var camera := $Camera as Camera

var hull := max_hull
var pitch_target := 0.0
var yaw_target := 0.0
var drift := 0.0
var velocity := Vector3()
var team := "blue"

func set_team(val: String):
	team = val
	add_to_group(val)

func _enter_tree():
	add_to_group("player")

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	propagate_call("set_team", ["blue"])

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		yaw_target -= event.relative.x * MOUSE_SENSITIVITY.x
		pitch_target -= event.relative.y * MOUSE_SENSITIVITY.y

func _physics_process(delta: float):
	var pitch := ((
		Input.get_action_strength("pitch_up") -
		Input.get_action_strength("pitch_down")
	) + pitch_target) * pitch_factor
	var yaw := ((
		Input.get_action_strength("yaw_left") -
		Input.get_action_strength("yaw_right")
	) + yaw_target) * yaw_factor
	var throttle := Input.get_action_strength("accelerate")
	var brake := Input.get_action_strength("brake")

	yaw_target = 0.0
	pitch_target = 0.0

	if throttle == 0:
		throttle = -brake

	var fwd = global_transform.basis.z.normalized()

	var roll_target := -yaw / yaw_factor * MAX_ROLL
	rotate(Vector3.UP, yaw * delta)
	rotate(global_transform.basis.x, pitch * delta)
	rotate(global_transform.basis.z, (roll_target - rotation.z) * ROLL_SPEED * delta)

	var target_speed := base_speed + (max_speed - base_speed) * throttle
	var speed := velocity.length()
	speed = speed + (target_speed - speed) * acceleration * delta
	drift = clamp(drift + brake - delta, 0.0, 0.95)
	velocity = speed * (
		drift * velocity.normalized() +
		(1.0 - drift) * fwd
	)

	move_and_collide(velocity)

	camera.look_at(global_transform.origin + velocity * delta * CAMERA_MOVE_FACTOR, Vector3.UP)
	camera.fov = 70 + (speed - base_speed) / base_speed * 20

	$PrimaryWeapon.active = Input.is_action_pressed("fire_primary")
