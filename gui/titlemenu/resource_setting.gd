extends Range

export(String) var property: String
export(Resource) var resource: Resource

func _ready():
	value = resource.get(property)
	if connect("value_changed", resource, "set_" + property):
		push_error("connect failed")
