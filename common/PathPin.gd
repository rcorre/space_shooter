tool
extends PathFollow
class_name PathPin

export(float, 0.001, 1) var pin := 0.001

onready var path: Path = get_parent()

func _ready():
	path.connect("curve_changed", self, "_on_curve_changed")

func _on_curve_changed():
	unit_offset = pin
