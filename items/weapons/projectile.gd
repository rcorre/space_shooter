extends RigidBody
class_name Projectile

signal hit

export(Vector3) var explosion_size := Vector3(0.2, 0.2, 0.2)
export(float, 0, 100) var damage_radius := 0.0

var damage := 10.0
var duration := 5.0
var team := "blue"

func _ready():
	if connect("body_entered", self, "_on_body_entered"):
		push_error("Failed to connect body_entered in projectile")

func set_team(val: String):
	team = val

func _on_body_entered(body: Node):
	Explosion.create(get_parent(), global_transform.origin, Vector3.ONE * explosion_size, team)
	queue_free()
	var did_hit := false
	if body.has_method("damage") and Global.is_hostile(body, team):
		body.damage(damage)
		did_hit = true

	if damage_radius > 0.0:
		for n in get_tree().get_nodes_in_group(Global.opposing_team(team)):
			if n != body and n.has_method("damage"):
				var dist = (n.global_transform.origin - global_transform.origin).length()
				if dist < damage_radius:
					n.damage(damage)
					did_hit = true

	if did_hit:
		emit_signal("hit")
