extends Spatial
class_name Weapon

signal hit

export(Texture) var icon: Texture
export(PackedScene) var projectile : PackedScene
export(float, 0, 1000) var damage := 10.0
export(float, 0, 60) var fire_rate := 6.0
export(float, 0, 1000) var projectile_speed := 100.0
export(float, 0, 90) var spread := 0.0
export(int, 0, 30) var shot_count := 1

onready var fire_point: Spatial = $FirePoint

var active := false
var fire_delay := 0.0
var team := "blue"

func set_team(val: String):
	team = val

func fire():
	var point := fire_point.global_transform as Transform

	for i in range(shot_count):
		var p = projectile.instance()
		p.damage = damage
		p.propagate_call("set_team", [team])
		p.global_transform = point

		var heading := point.basis.z
		heading = heading.rotated(point.basis.x, deg2rad(rand_range(0, spread)))
		heading = heading.rotated(point.basis.z, rand_range(0, TAU))
		p.linear_velocity = projectile_speed * heading

		get_tree().root.add_child(p)
		p.connect("hit", self, "emit_signal", ["hit"])

func _physics_process(delta: float):
	fire_delay = max(0, fire_delay - delta)
	if active and fire_delay <= 0:
		fire()
		fire_delay = 1.0 / fire_rate
