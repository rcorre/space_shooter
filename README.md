# Guardian

# Software Used:

- [Godot](https://godotengine.org/)
- [Blender](https://www.blender.org/)
- [Inkscape](https://inkscape.org/)
- [Audacity](https://www.audacityteam.org/)
- [Archlinux](https://www.archlinux.org/)
- [Neovim](https://neovim.io/)

# Credits

## Fonts

This game uses the [Orbitron](https://www.theleagueofmoveabletype.com/orbitron) font,
licensed under the [Open Font License](./common/Open Font License.markdown).

## Game Icons

This game uses several icons from [game-icons.net](https://game-icons.net):

## Freesound

The following sounds from freesound were used:
