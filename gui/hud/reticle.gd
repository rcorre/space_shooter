extends RayCast

signal enemy_targeted
signal ally_targeted
signal nothing_targeted
signal targeted(entity)

export var default_distance := 30.0

var team: String
var target: Node

func set_team(val: String):
	team = val

func _physics_process(delta):
	$MeshInstance.visible = is_colliding()
	if not is_colliding():
		return
	$MeshInstance.global_transform.origin = get_collision_point()
	var collider := get_collider() as Node
	if collider != target:
		target = collider
		var enemy := "blue" if team == "red" else "red"
		if collider.is_in_group(team):
			emit_signal("ally_targeted")
			emit_signal("targeted", target)
		elif collider.is_in_group(enemy):
			emit_signal("enemy_targeted")
			emit_signal("targeted", target)
		else:
			emit_signal("nothing_targeted")

func show_hit_marker():
	$HitAnimator.stop()
	$HitAnimator.play("hit_marker")
