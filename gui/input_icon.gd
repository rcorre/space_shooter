extends Control

const KEY_TEXTURE: Texture = preload("res://gui/keyboard_key.svg")
const JOY_TEXTURE: Texture = preload("res://gui/joy_button.svg")

onready var key := $Key as Label
onready var joy_button := $JoyButton as Label
onready var mouse_button := $MouseButton as Sprite

export(String, "", "accelerate", "brake", "pitch_up", "pitch_down", "yaw_left", "yaw_right", "fire_primary", "fire_secondary", "move_forward", "move_back", "move_left", "move_right", "hover_up", "hover_down", "next_unit", "drop") var action := ""

func _ready():
	var events := InputMap.get_action_list(action)
	if not events:
		hide()
		return

	for c in get_children():
		c.hide()

	var ev := events.front() as InputEvent
	if ev is InputEventMouseButton:
		mouse_button.frame = ev.button_index - 1
		mouse_button.show()
	elif ev is InputEventJoypadButton:
		joy_button.show()
	elif ev is InputEventKey:
		key.text = ev.as_text()
		key.show()
	else:
		printerr("Unknown event %s for %s" % [ev, action])
	show()
