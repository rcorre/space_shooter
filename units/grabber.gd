extends Area

signal grabbed(body)
signal dropped(body)
signal emptied(is_empty)

const RECONNECT_TIME := 1.0

var team: String

func set_team(val: String):
	team = val

func _connect_signal():
	if connect("body_entered", self, "_on_body_entered", [], CONNECT_ONESHOT | CONNECT_DEFERRED):
		push_error("connect failed")

func release_body(body):
	get_tree().create_timer(RECONNECT_TIME).connect("timeout", self, "_connect_signal")
	emit_signal("dropped", body)
	emit_signal("emptied", true)
	body.propagate_call("set_held", [false])

func _ready():
	_connect_signal()

func _on_body_entered(body: Node):
	if not Global.is_hostile(body, team):
		var parent := body.get_parent()
		parent.remove_child(body)
		body.transform = Transform.IDENTITY
		add_child(body)
		body.connect("tree_exiting", self, "release_body", [body], CONNECT_ONESHOT)
		emit_signal("grabbed", body)
		emit_signal("emptied", false)
		body.propagate_call("set_held", [true])
	else:
		_connect_signal()

func drop():
	for c in get_children():
		var body := c as PhysicsBody
		if body:
			var trans := body.global_transform
			remove_child(body)
			get_tree().current_scene.add_child(body)
			c.global_transform = trans
