extends Button

export(String, "accelerate", "brake", "pitch_up", "pitch_down", "yaw_left", "yaw_right", "fire_primary", "fire_secondary", "move_forward", "move_back", "move_left", "move_right", "hover_up", "hover_down") var action := ""

func _ready():
	text = action.capitalize()
	#connect("button_down", self, "set_text", [action.capitalize() + " |"])

func _gui_input(event: InputEvent):
	if pressed and event.is_action_type():
		pressed = false
		for old in InputMap.get_action_list(action):
			InputMap.action_erase_event(action, old)
		InputMap.action_add_event(action, event)
		accept_event()
