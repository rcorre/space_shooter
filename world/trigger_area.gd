extends Area
class_name TriggerArea

signal tripped
signal left

func _ready():
	connect("body_entered", self, "_on_body_entered")
	connect("body_entered", self, "_on_body_exited")

func _on_body_entered(_node: Node):
	emit_signal("tripped")

func _on_body_exited(_node: Node):
	emit_signal("left")
