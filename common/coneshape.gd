tool
extends ConvexPolygonShape
class_name ConeShape

export(float, EXP, 0, 1000, 0.1) var start_size := 1.0 setget set_start_size, get_start_size
export(float, EXP, 0, 1000, 0.1) var end_size := 2.0 setget set_end_size, get_end_size
export(float, EXP, 0, 1000, 0.1) var length := 5.0 setget set_length, get_length

func set_start_size(val: float):
	start_size = val
	update_shape()

func get_start_size():
	return start_size

func set_end_size(val: float):
	end_size = val
	update_shape()

func get_end_size():
	return end_size

func set_length(val: float):
	length = val
	update_shape()

func get_length():
	return length

func _init():
	._init()
	resource_name = "ConeShape"
	update_shape()

func update_shape():
	set_points([
		Vector3(-end_size, length, end_size),
		Vector3(end_size, length, -end_size),
		Vector3(end_size, length, end_size),
		Vector3(-end_size, length, -end_size),
		Vector3(-start_size, 0, start_size),
		Vector3(start_size, 0, -start_size),
		Vector3(start_size, 0, start_size),
		Vector3(-start_size, 0, -start_size)
	])
