extends Projectile

export(float, 0, 1000) var rocket_force := 100.0

var target: Spatial

func set_target(s: Spatial):
	target = s

func _physics_process(delta: float):
	if is_instance_valid(target):
		var dir := global_transform.origin.direction_to(target.global_transform.origin)
		add_central_force(dir * rocket_force)
	else:
		add_central_force(global_transform.basis.z.normalized() * rocket_force)
