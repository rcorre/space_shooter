All icons in this folder come from [game-icons.net](https://game-icons.net) and are licensed under [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/) unless otherwise noted.
