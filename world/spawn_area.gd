extends Spatial

const drop_pod_scene = preload("res://units/drop_pod/pod_small.tscn")

export(String) var team := "red"
export(PackedScene) var unit: PackedScene
export(float, 0, 1000) var spawn_height := 300
export(float, 0, 10) var spawn_delay := 1.0
export(int, 0, 1000) var challenge := 10

onready var bounds := $SpawnBounds as CollisionShape

func _ready():
	if connect("body_entered", self, "_on_body_entered", [], CONNECT_ONESHOT):
		push_error("connect failed")

func _on_body_entered(body: Node):
	if $Timer.connect("timeout", self, "spawn_unit"):
		push_error("connect failed")
	$Timer.start(spawn_delay)

func spawn_unit():
	var pod := drop_pod_scene.instance()
	pod.unit_scene = unit
	get_parent().add_child(pod)
	var shape := bounds.shape as BoxShape
	var extents := shape.extents
	pod.global_transform.origin = bounds.global_transform.origin + Vector3(
		rand_range(-extents.x, extents.x),
		rand_range(-extents.y, extents.y) + spawn_height,
		rand_range(-extents.z, extents.z)
	)
	pod.global_transform.basis = global_transform.basis
	pod.propagate_call("set_team", [team])

	challenge -= 1
	if challenge <= 0:
		queue_free()
