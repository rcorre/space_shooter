extends PathFollow
class_name TrainMotion

var speed: float

func _ready():
	rotation_mode = ROTATION_ORIENTED

func _physics_process(delta: float):
	offset += speed * delta
