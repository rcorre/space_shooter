extends Control
class_name HUD

onready var heat_bar := find_node("HeatBar")
onready var health_bar := find_node("HealthBar")
onready var shield_bar := find_node("ShieldBar")

func set_health(val: float, max_val: float):
	health_bar.value = val
	health_bar.max_value = max_val

func set_shield(val: float, max_val: float):
	shield_bar.value = val
	shield_bar.max_value = max_val

func set_heat(val: float, max_val: float):
	heat_bar.value = val
	heat_bar.max_value = max_val
	heat_bar.modulate.r = val / max_val

func set_player(player: Spatial):
	find_node("Minimap").player = player
