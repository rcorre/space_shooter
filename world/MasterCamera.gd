extends InterpolatedCamera

onready var tween: Tween = $Tween

func _ready():
	Events.connect("camera_assigned", self, "_on_camera_assigned")

func _on_camera_assigned(path: NodePath, new_speed: float):
	target = path
	tween.interpolate_property(self, ":speed", speed, new_speed, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.start()

func _process(_delta: float):
	var cam := get_node(target) as Camera
	if cam:
		cull_mask = cam.cull_mask
