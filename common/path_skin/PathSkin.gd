tool
extends ImmediateGeometry
class_name PathSkin

export(float, EXP, 0.01, 100.0) var step := 0.1 setget set_step
export(float, 0, 1) var end := 1.0 setget set_end
export(Vector3) var offset := Vector3(0, 1, 0) setget set_offset

onready var path: Path = get_parent()

func set_offset(val: Vector3):
	offset = val
	skin()

func set_step(val: float):
	step = val
	skin()

func set_end(val: float):
	end = val
	skin()

func _ready():
	path.connect("curve_changed", self, "skin")
	call_deferred("skin")

func skin():
	clear()
	if path and path.curve and path.curve.get_point_count() > 1:
		var follower := PathFollow.new()
		follower.loop = false
		path.add_child(follower)
		begin(Mesh.PRIMITIVE_LINE_STRIP)
		while follower.unit_offset < end:
			follower.offset += step
			add_vertex(follower.transform.origin + offset)
		end()
		follower.queue_free()
