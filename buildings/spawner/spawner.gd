extends Spatial

export(PackedScene) var UNIT_SCENE

var team := "neutral"

func set_team(val: String):
	team = val
	if team == "neutral":
		$SpawnTimer.stop()
	else:
		$SpawnTimer.start()

func spawn_unit():
	var unit := UNIT_SCENE.instance() as Spatial
	get_parent().add_child(unit)
	unit.propagate_call("set_team", [team])
	unit.global_transform = $SpawnPoint.global_transform
