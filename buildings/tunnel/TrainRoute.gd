tool
extends Path
class_name TrainRoute

const CAR_COUNT := 3
const CAR_SPACING := 18.0
const CAR_SCENE: PackedScene = preload("res://units/train/traincar.tscn")

export(float, EXP, 0, 100) var speed := 12.0

var cars_alive := 0

func spawn():
	var car := CAR_SCENE.instance()
	var motion := TrainMotion.new()
	motion.add_child(car)
	add_child(motion)
	motion.speed = speed
	car.connect("tree_exiting", self, "_on_car_destroyed", [motion])
	cars_alive += 1
	if cars_alive < CAR_COUNT:
		yield(get_tree().create_timer(CAR_SPACING / speed), "timeout")
		spawn()

func _on_car_destroyed(motion: Node):
	motion.queue_free()
	cars_alive -= 1
	if cars_alive <= 0:
		spawn()
