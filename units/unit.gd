extends KinematicBody
class_name Unit

signal destroyed

const CAMERA_MOVE_FACTOR := 0.0
const MAX_ROLL := PI / 8
const PITCH_RATE := 1.5
const MOUSE_SENSITIVITY := Vector2(0.05, 0.1)
const JOY_SENSITIVITY := Vector2(1.5, 0.5)

export(float) var max_elevation := 50.0
export(Vector3) var max_velocity := Vector3(20, 30, 30)
export(Vector3) var acceleration := Vector3(40, 60, 40)
export(float) var damping := 0.8
export(float) var gravity := 9.8
export(float) var max_health := 100.0
export(float, 0, 1.57) var max_pitch_down := PI / 8
export(float, 0, 1.57) var max_pitch_up := PI / 8

onready var camera := $Camera as Camera
onready var crosshairs := $CrosshairViewport/Crosshairs as Crosshairs
onready var hud := $HUD as HUD
onready var weapon: Weapon
onready var body: MeshInstance = $Mesh
onready var reticle: Node = $Mesh/Reticle
onready var shield := $Mesh/Shield
onready var swap_tooltip: Spatial = $SwapToolTip
onready var grabber: Area = $GrabArea

var health := 100.0
var turn_target := 0.0
var pitch_target := 0.0
var velocity := Vector3()
var team := "blue"
var active := false setget set_active

func set_active(val: bool):
	active = val
	hud.visible = active
	crosshairs.visible = active
	reticle.visible = active
	swap_tooltip.visible = not val
	if active:
		Events.emit_signal("camera_assigned", camera.get_path(), 7)
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func set_team(val: String):
	team = val
	add_to_group(val)

func damage(amount: float):
	amount = shield.absorb(amount)
	health -= amount
	crosshairs.set_health(health)
	if health <= 0:
		Explosion.create(get_parent(), global_transform.origin, Vector3(3, 3, 3), team)
		queue_free()
		remove_from_group("player_selectable")
		emit_signal("destroyed")
		cycle_unit()

func cycle_unit():
	set_active(false)
	var units := get_tree().get_nodes_in_group("player_selectable")
	# TODO: gameover
	if units:
		var next = units[(units.find(self) + 1) % len(units)]
		next.set_active(true)

func _enter_tree():
	add_to_group("player")
	add_to_group("player_selectable")

func _ready():
	propagate_call("set_team", ["blue"])
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	hud.set_player(self)

func _input(event):
	if not active:
		return

	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		turn_target -= event.relative.x * MOUSE_SENSITIVITY.x
		pitch_target = clamp(pitch_target - event.relative.y * MOUSE_SENSITIVITY.y, -max_pitch_up, max_pitch_up)

	if event.is_action("fire_primary") and weapon:
		print('fire primary')
		weapon.active = event.is_pressed()
	elif event.is_action_pressed("next_unit"):
		cycle_unit()
	elif event.is_action_pressed("drop"):
		grabber.drop()
	else:
		return

	get_tree().set_input_as_handled()

func _physics_process(delta: float):
	var rel_vel = global_transform.basis.xform_inv(velocity)
	if active:
		rel_vel.x += (
			Input.get_action_strength("move_left") -
			Input.get_action_strength("move_right")
		) * acceleration.x * delta
		rel_vel.y += (
			Input.get_action_strength("hover_up") -
			Input.get_action_strength("hover_down")
		) * acceleration.y * delta
		rel_vel.z += (
			Input.get_action_strength("move_forward") -
			Input.get_action_strength("move_back")
		) * acceleration.z * delta

	var pitch_factor = body.rotation.x / max_pitch_up

	rel_vel.x = clamp(rel_vel.x, -max_velocity.x, max_velocity.x)
	rel_vel.y = clamp(rel_vel.y, -max_velocity.y, max_velocity.y)
	rel_vel.z = clamp(
		rel_vel.z,
		-max_velocity.z + pitch_factor * 20,
		max_velocity.z + pitch_factor * 20
	)

	velocity = global_transform.basis.xform(rel_vel)
	velocity -= velocity * damping * delta
	if not is_on_floor():
		velocity.y -= gravity * delta

	if global_transform.origin.y > max_elevation:
		velocity.y = min(velocity.y, velocity.y / (global_transform.origin.y / max_elevation))

	if active:
		turn_target += (Input.get_action_strength("look_left") - Input.get_action_strength("look_right")) * JOY_SENSITIVITY.x
		pitch_target += (Input.get_action_strength("look_up") - Input.get_action_strength("look_down")) * JOY_SENSITIVITY.y
	rotate(Vector3.UP, turn_target * delta)
	body.rotation.x = clamp(body.rotation.x + pitch_target * delta * PITCH_RATE, -max_pitch_up, max_pitch_down)
	body.rotation.z = -MAX_ROLL * rel_vel.x / max_velocity.x

	turn_target = 0.0
	pitch_target = 0.0

	velocity = move_and_slide(velocity)

	#camera.look_at(global_transform.origin + velocity * delta * CAMERA_MOVE_FACTOR, Vector3.UP)
	#camera.fov = 70 + (speed - base_speed) / base_speed * 20

func _process(delta: float):
	hud.set_shield(shield.health, shield.max_health)
	hud.set_health(health, max_health)

	if shield.health > 0:
		crosshairs.set_shield(shield.health)
	else:
		crosshairs.set_health(health)

func repair(amount: float):
	health = min(health + amount, max_health)
	crosshairs.set_health(health)

func _on_object_grabbed(obj: Node):
	add_collision_exception_with(obj)
	camera.set_cull_mask_bit(Global.RenderLayer.GRAB, false)
	var w := obj as Weapon
	if w:
		prints('equip weapon', w)
		weapon = w
		# w.connect("hit", reticle, "show_hit_marker")
	elif obj.is_in_group("token"):
		camera.set_cull_mask_bit(Global.RenderLayer.BUILD, true)

func _on_object_dropped(obj: Node):
	remove_collision_exception_with(obj)
	camera.set_cull_mask_bit(Global.RenderLayer.GRAB, true)
	var w := obj as Weapon
	if w:
		#w.disconnect("hit", reticle, "show_hit_marker")
		weapon = null
		camera.set_cull_mask_bit(Global.RenderLayer.BUILD, true)
	elif obj.is_in_group("token"):
		camera.set_cull_mask_bit(Global.RenderLayer.BUILD, false)
