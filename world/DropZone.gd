tool
extends Spatial

const POD_HEIGHT := 300
const AIR_HEIGHT := 20
const UNITS = [{
		challenge=1,
		unit=preload("res://units/tank/Tank.tscn"),
		pod=preload("res://units/drop_pod/pod_small.tscn"),
	}, {
		challenge=1,
		unit=preload("res://units/moth/Moth.tscn"),
	}
]

export(BoxShape) var shape: BoxShape
export(String) var team := "red"

onready var im = $ImmediateGeometry

func _ready():
	Events.connect("wave_started", self, "start_wave")

func set_team(val: String):
	team = val

func _process(delta):
	if not shape or not Engine.editor_hint:
		return

	var a := -shape.extents / 2
	var b := shape.extents / 2

	im.clear()
	im.begin(Mesh.PRIMITIVE_LINES)
	im.add_vertex(Vector3(a.x, a.y, a.z))
	im.add_vertex(Vector3(b.x, a.y, a.z))

	im.add_vertex(Vector3(a.x, a.y, a.z))
	im.add_vertex(Vector3(a.x, b.y, a.z))

	im.add_vertex(Vector3(a.x, a.y, a.z))
	im.add_vertex(Vector3(a.x, a.y, b.z))

	im.add_vertex(Vector3(b.x, b.y, b.z))
	im.add_vertex(Vector3(a.x, b.y, b.z))

	im.add_vertex(Vector3(b.x, b.y, b.z))
	im.add_vertex(Vector3(b.x, a.y, b.z))

	im.add_vertex(Vector3(b.x, b.y, b.z))
	im.add_vertex(Vector3(b.x, b.y, a.z))

	im.add_vertex(Vector3(b.x, b.y, b.z))
	im.add_vertex(Vector3(b.x, b.y, a.z))

	im.add_vertex(Vector3(b.x, b.y, b.z))
	im.add_vertex(Vector3(b.x, b.y, a.z))

	im.add_vertex(Vector3(a.x, a.y, b.z))
	im.add_vertex(Vector3(a.x, b.y, b.z))

	im.add_vertex(Vector3(a.x, b.y, a.z))
	im.add_vertex(Vector3(a.x, b.y, b.z))

	im.add_vertex(Vector3(a.x, b.y, a.z))
	im.add_vertex(Vector3(b.x, b.y, a.z))

	im.add_vertex(Vector3(a.x, a.y, b.z))
	im.add_vertex(Vector3(b.x, a.y, b.z))

	im.add_vertex(Vector3(b.x, a.y, a.z))
	im.add_vertex(Vector3(b.x, b.y, a.z))

	im.add_vertex(Vector3(b.x, a.y, a.z))
	im.add_vertex(Vector3(b.x, a.y, b.z))
	im.end()

func spawn(challenge: int, tier: int):
	var info = UNITS[randi() % int(min(tier, len(UNITS)))]
	var unit: Node
	var spawn_height: float
	if info.has("pod"):
		unit = info.pod.instance()
		unit.unit_scene = info.unit
		spawn_height = POD_HEIGHT
	else:
		unit = info.unit.instance()
		spawn_height = AIR_HEIGHT
	get_parent().add_child(unit)
	var extents := shape.extents
	unit.global_transform.origin = global_transform.origin + Vector3(
		rand_range(-extents.x, extents.x),
		rand_range(-extents.y, extents.y) + spawn_height,
		rand_range(-extents.z, extents.z)
	)
	unit.global_transform.basis = global_transform.basis
	unit.propagate_call("set_team", [team])

	if challenge > 0:
		yield(get_tree().create_timer(1), "timeout")
		spawn(challenge - info.challenge, tier)
