extends Spatial

signal hit

onready var raycast := $RayCast as RayCast
onready var mesh := $MeshInstance as MeshInstance

var damage: float
var linear_velocity: Vector3
var team: String

func set_team(val: String):
	team = val

func _physics_process(delta: float):
	if not raycast.enabled:
		return
	var dist := raycast.get_collision_point().distance_to(global_transform.origin)
	mesh.translation.z = dist / 2
	mesh.scale.z = dist

	var target = raycast.get_collider()
	if target and Global.is_hostile(target, team) and target.has_method("damage"):
		target.damage(damage)
		emit_signal("hit")
	raycast.enabled = false
