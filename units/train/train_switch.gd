extends Spatial

signal tripped

export(float, -100, 100) var speed := 0.0

func _on_body_entered(body: Node):
	var motion := body.get_parent() as TrainMotion
	motion.set_speed(speed)
	emit_signal("tripped")
