extends Entity
class_name Building

onready var anim_player: AnimationPlayer = $AnimationPlayer

func build():
	health = MAX_HEALTH
	anim_player.play("Build")
