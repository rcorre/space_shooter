extends ImmediateGeometry

func set_target(target: Spatial):
	var c := Curve3D.new()
	c.add_point(Vector3.ZERO, Vector3.ZERO, Vector3(0, 5, 0))
	c.add_point(to_local(target.global_transform.origin))
	begin(Mesh.PRIMITIVE_LINE_STRIP)
	for i in range(0, 11):
		add_vertex(c.interpolatef(i / 10.0) + Vector3(randf(), randf(), randf()))
	end()
