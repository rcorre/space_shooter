extends Spatial
class_name ItemDrop

const UPGRADE_SCENE = preload("res://units/upgrade/Upgrade.tscn")
const ITEMS = [
	preload("res://items/weapons/auto/data.tres"),
	preload("res://items/weapons/flak/data.tres"),
	preload("res://items/weapons/beam/data.tres"),
]

export(float, 0, 1, 0.01) var probability := 0.1

func _ready():
	get_parent().connect("destroyed", self, "_on_parent_destroyed")

func _on_parent_destroyed():
	if probability > randf():
		var obj
		obj = UPGRADE_SCENE.instance()
		obj.weapon = ITEMS[randi() % len(ITEMS)]
		get_tree().current_scene.add_child(obj)
		obj.global_transform = global_transform
