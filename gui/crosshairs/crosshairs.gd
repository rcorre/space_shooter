extends Control
class_name Crosshairs

signal shield_set(val)
signal health_set(val)

func set_shield(val: float):
	emit_signal("shield_set", val)
	emit_signal("health_set", 0)

func set_health(val: float):
	emit_signal("health_set", val)
	emit_signal("shield_set", 0)
