extends Spatial

const DROP_POD = preload("res://units/drop_pod/pod_small.tscn")

export(PackedScene) var UNIT: PackedScene = preload("res://units/spider/spider.tscn")
export(float, 0, 30) var RADIUS := 30.0
export(float, 0, 120) var COOLDOWN := 15.0
export(float, 0, 1000) var SPAWN_HEIGHT := 300
export(int, 0, 12) var COUNT := 3

onready var spawn_timer: Timer = $SpawnTimer
onready var cooldown_timer: Timer = $CooldownTimer
onready var use_sound: AudioStreamPlayer = $UseSound
onready var error_sound: AudioStreamPlayer = $ErrorSound
onready var regen_sound: AudioStreamPlayer = $RegenSound

var usable := true

func use(user: Spatial):
	if usable:
		usable = false
		use_sound.play()
		spawn(user, COUNT)
		cooldown_timer.start()
		yield(cooldown_timer, "timeout")
		usable = true
		regen_sound.play()
	else:
		error_sound.play()

func spawn(user: Spatial, remaining: int):
	var pod := DROP_POD.instance()
	pod.unit_scene = UNIT
	user.get_parent().add_child(pod)
	pod.global_transform.origin = global_transform.origin + Vector3(
		rand_range(-RADIUS, RADIUS),
		rand_range(-RADIUS, RADIUS) + SPAWN_HEIGHT,
		rand_range(-RADIUS, RADIUS)
	)
	pod.global_transform.basis = global_transform.basis
	pod.propagate_call("set_team", [user.team])
	if remaining > 0:
		spawn_timer.start()
		yield(spawn_timer, "timeout")
		spawn(user, remaining - 1)
	else:
		spawn_timer.stop()
