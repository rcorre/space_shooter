extends Spatial

signal target_changed(target)

export(float, 0, 200) var target_range := 100.0
export(float, 0, 10) var retarget_time := 1.0
export(float, 0, 10) var burst_time := 1.0

onready var weapon: Weapon = get_child(0)

var team: String
var target: Spatial

func _ready():
	var retarget_timer := Timer.new()
	if retarget_timer.connect("timeout", self, "retarget"):
		push_error("connect failed")
	add_child(retarget_timer)
	retarget_timer.start(retarget_time)

	var burst_timer := Timer.new()
	add_child(burst_timer)
	retarget_timer.connect("timeout", self, "toggle_firing")
	burst_timer.wait_time = burst_time
	burst_timer.start()

func set_team(t: String):
	team = t

func _physics_process(_delta: float):
	# typecheck shouldn't be necessary, but see
	# https://github.com/godotengine/godot/issues/34860
	if is_instance_valid(target):
		var pos := target.global_transform.origin
		if target.has_node("TargetPoint"):
			pos = target.get_node("TargetPoint").global_transform.origin
		look_at(pos, Vector3(0, 1, 0))

func retarget():
	var target_group = "blue" if team == "red" else "red"

	target = null
	var target_dist = target_range
	for n in get_tree().get_nodes_in_group(target_group):
		var dist = global_transform.origin.distance_to(n.global_transform.origin)
		if dist < target_dist:
			target_dist = dist
			target = n

	emit_signal("target_changed", target)

func toggle_firing():
	weapon.active = not weapon.active and is_instance_valid(target)
