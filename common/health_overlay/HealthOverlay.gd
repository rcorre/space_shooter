extends Spatial

onready var unit: Entity = get_parent()
onready var bar: ProgressBar = $Viewport/ProgressBar

func _process(delta):
	bar.value = 100 * unit.health / unit.MAX_HEALTH
