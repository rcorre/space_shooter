extends Control

onready var anim_player: AnimationPlayer = $AnimationPlayer

func start_countdown():
	show()
	anim_player.play("Countdown")
