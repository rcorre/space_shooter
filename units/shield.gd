extends Node

export(float, 0, 1000) var max_health := 100.0
export(float, 0, 10) var recharge_delay := 4.0
export(float, 0, 1000) var recharge_rate := 20.0

var until_recharge := 0.0
var health := max_health

func absorb(damage: float) -> float:
	until_recharge = recharge_delay

	if health <= 0.0:
		return damage

	health -= damage
	if health <= 0:
		$AnimationPlayer.play("break")
		var remainder := abs(health)
		health = 0
		return remainder
	else:
		$AnimationPlayer.stop()
		$AnimationPlayer.play("absorb")

	return 0.0  # no unabsorbed damage

func _physics_process(delta: float):
	until_recharge = max(until_recharge - delta, 0)
	if until_recharge <= 0:
		if health <= 0:
			$AnimationPlayer.play("regen")
		health = min(health + delta * recharge_rate, max_health)
