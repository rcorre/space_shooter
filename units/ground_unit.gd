extends KinematicBody

signal destroyed

export(float, 0, 1000) var MAX_HEALTH := 100.0
export(float, 0, 10) var turn_rate := 1.0

onready var health := MAX_HEALTH
onready var anim_tree := $AnimationTree as AnimationTree

var team := "red"
var target: Spatial
var velocity: Vector3

func set_team(val: String):
	team = val
	add_to_group(val)

func set_target(val: Spatial):
	target = val

func _physics_process(delta: float):
	anim_tree["parameters/move/blend_position"] = 1.0

	var root_motion := anim_tree.get_root_motion_transform()
	global_transform.basis *= root_motion.basis

	if not is_on_floor():
		velocity += Global.GRAVITY * delta

	var movement := global_transform.basis.xform(root_motion.origin) / delta
	velocity.x = movement.x
	velocity.z = movement.z
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))

func damage(amount: float):
	health -= amount
	if health <= 0.0:
		queue_free()
		Explosion.create(get_parent(), global_transform.origin, Vector3(2, 4, 2), team)
		emit_signal("destroyed")

func face(dir: Basis):
	# TODO: lerp
	global_transform.basis = dir.orthonormalized()
