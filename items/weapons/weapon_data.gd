extends Resource
class_name WeaponData

export(String) var name: String
export(Texture) var icon: Texture
export(PackedScene) var projectile : PackedScene
export(float, 0, 1000) var damage := 10.0
export(float, 0, 60) var fire_rate := 6.0
export(float, 0, 1000) var projectile_speed := 100.0
export(float, 0, 100) var projectile_duration := 5.0
export(float, 0, 90) var spread := 0.0
export(int, 0, 30) var shot_count := 1
