tool
extends Spatial
class_name ToolTip3D

signal message_changed

export(String) var message : String setget set_message
export(int, FLAGS, "Default", "Build", "Base", "Grab", "Player", "Upgrade") var layers: int = Global.RenderLayer.DEFAULT

func set_message(val: String):
	message = val
	emit_signal("message_changed", val)

func _ready():
	$MeshInstance.layers = layers

func fade():
	$AnimationPlayer.play("fade")
