extends Resource
class_name UnitData

export(String) var name: String
export(Texture) var icon: Texture
export(PackedScene) var scene: PackedScene
export(int) var health := 100
export(int) var shield := 100
