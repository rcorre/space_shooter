extends Spatial

signal hit

export(float, 0, 100) var speed := 10.0

var damage: float
var linear_velocity: Vector3
var team: String
var target: Spatial

func set_team(val: String):
	team = val

func _on_chasearea_entered(body: Node):
	if Global.is_hostile(body, team):
		target = body as Spatial

func _on_chasearea_exited(body: Node):
	if body == target:
		target = null

func _on_area_entered(body: Node):
	if not Global.is_hostile(body, team):
		return
	var did_hit := false
	var radius := $Area/CollisionShape.shape.radius as float
	Explosion.create(get_parent(), global_transform.origin, Vector3.ONE * radius, team)
	queue_free()
	for n in get_tree().get_nodes_in_group(Global.opposing_team(team)):
		if n != body and n.has_method("damage"):
			var dist = (n.global_transform.origin - global_transform.origin).length()
			if dist < radius:
				n.damage(damage)
				did_hit = true

	if did_hit:
		emit_signal("hit")

func _physics_process(delta: float):
	if target:
		global_translate(speed * delta * global_transform.origin.direction_to(target.global_transform.origin))
