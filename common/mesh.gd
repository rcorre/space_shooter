extends MeshInstance

export(int) var themeable_slot = 0
export(String, "dim", "bright") var kind := "dim"

func set_team(team: String):
	assert(team == "blue" or team == "red" or team == "neutral")
	var mat = load("res://assets/materials/%s/%s.tres" % [team, kind])
	set_surface_material(themeable_slot, mat)
