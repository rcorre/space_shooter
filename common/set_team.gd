extends Spatial

export(String, "red", "blue", "neutral") var team := "red"

func _ready():
	propagate_call("set_team", [team])
