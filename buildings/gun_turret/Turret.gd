extends Entity

onready var weapon: Weapon = find_node("Weapon")

func _ready():
	connect("destroyed", self, "_on_destroyed")

func _on_destroyed():
	Explosion.create(get_parent(), global_transform.origin, Vector3(6, 6, 6), team)
	queue_free()

func equip(data: WeaponData):
	weapon.data = data
