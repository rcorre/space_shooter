extends Projectile

export(PackedScene) var effect: PackedScene

func set_target(target: Spatial):
	target.damage(damage)
	emit_signal("hit")

	var e := effect.instance()
	get_parent().add_child(e)
	e.global_transform.origin = global_transform.origin
	e.propagate_call("set_team", [team])
	e.set_target(target)
	queue_free()
