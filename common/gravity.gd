extends Node
class_name Gravity

export(float, EXP, 0, 100) var gravity := 9.8

onready var body: KinematicBody = get_parent()

var velocity: Vector3
var held := false

func _ready():
	set_physics_process(false)

func _physics_process(delta: float):
	if not held:
		if not body.is_on_floor():
			velocity.y -= gravity * delta
		velocity = body.move_and_slide(velocity, Vector3.UP)

func set_held(val: bool):
	set_physics_process(not val)
	held = val
