extends Area

signal target_changed(target)

var team: String

func set_team(t: String):
	team = t

func _ready():
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body: Node):
	if Global.is_hostile(body, team) and body is Spatial:
		emit_signal("target_changed", body)
