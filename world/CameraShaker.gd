extends Spatial
class_name CameraShaker

export(float, EXP, 0, 1000) var range_factor := 500.0
export(float, EXP, 0, 10) var intensity := 1.0

func shake():
	for n in get_tree().get_nodes_in_group(CameraShake.GROUP):
		var s: CameraShake = n as CameraShake
		var dist := global_transform.origin.distance_to(s.global_transform.origin)
		var factor := clamp(1 - dist / range_factor, 0, 1)
		s.shake(factor * intensity)
