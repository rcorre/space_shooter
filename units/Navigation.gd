extends Spatial

export(float, 0, 20) var move_speed := 5.0
export(float, 0, 10) var turn_speed := 2.0
export(float, EXP, 0, 100) var gravity := 9.8

onready var ray_l: RayCast = $LeftRay
onready var ray_r: RayCast = $RightRay
onready var unit: KinematicBody = get_parent()

var velocity: Vector3

func _ray_depth(r: RayCast) -> float:
	if r.is_colliding():
		return r.get_collision_point().distance_to(r.global_transform.origin)
	return r.cast_to.z

func _physics_process(delta: float):
	var l := _ray_depth(ray_l)
	var r := _ray_depth(ray_r)

	unit.rotate_y(delta * turn_speed * sign(l - r))
	var motion := unit.global_transform.basis.z * move_speed
	velocity.x = motion.x
	velocity.y -= gravity * delta
	velocity.z = motion.z
	velocity = unit.move_and_slide(velocity)
