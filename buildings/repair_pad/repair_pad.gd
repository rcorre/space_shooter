extends Spatial

signal started
signal stopped

export var repair_rate := 20.0

var team := "blue"
var target : Spatial

func set_team(val: String):
	team = val

func _ready():
	if connect("body_entered", self, "_on_body_entered"):
		push_error("connect failed")
	if connect("body_exited", self, "_on_body_exited"):
		push_error("connect failed")

func _physics_process(delta: float):
	if target:
		target.repair(repair_rate * delta)

func _on_body_entered(body: Node):
	if body.is_in_group(team):
		target = body
		emit_signal("started")

func _on_body_exited(body: Node):
	if body.is_in_group(team):
		target = null
		emit_signal("stopped")
