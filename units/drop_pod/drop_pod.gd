extends KinematicBody

onready var anim_player: AnimationPlayer = $AnimationPlayer
onready var drop_sound: AudioStreamPlayer = $DropSound
onready var land_sound: AudioStreamPlayer = $LandSound
onready var camera_shaker: CameraShaker = $CameraShaker

var velocity := 100 * Vector3.DOWN

var unit_scene
var team := "red"

func set_team(val: String):
	team = val

func _physics_process(delta):
	var collision := move_and_collide(velocity * delta)
	if collision and collision.collider is StaticBody:
		velocity = Vector3.ZERO
		anim_player.play("Land")
		drop_sound.stop()
		land_sound.play()
		camera_shaker.shake()

func spawn_unit():
	var unit := unit_scene.instance() as Spatial
	get_parent().add_child(unit)
	unit.propagate_call("set_team", [team])
	unit.global_transform = global_transform
	if get_tree().create_timer(10.0).connect("timeout", self, "queue_free"):
		push_error("connect failed")
