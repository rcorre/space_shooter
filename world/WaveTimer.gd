extends Timer

export(int, 0, 50) var challenge := 5
export(int, 0, 10) var challenge_growth := 3

func _on_timeout():
	print_debug("starting wave with challenge ", challenge)
	Events.emit_signal("wave_started", challenge)
	challenge += challenge_growth

func _ready():
	connect("timeout", self, "_on_timeout")
	_on_timeout()
	start()
