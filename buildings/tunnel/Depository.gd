extends Spatial

const TOKEN_SCENE: PackedScene = preload("res://units/token/Token.tscn")

func _on_train_arrived(train: Node):
	for p in get_children():
		if not p.get_child_count():
			p.add_child(TOKEN_SCENE.instance())
			train.queue_free()
			return
