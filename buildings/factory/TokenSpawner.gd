extends Node

const TOKEN_SCENE: PackedScene = preload("res://units/token/Token.tscn")

func spawn():
	if get_child_count() == 0:
		var token := TOKEN_SCENE.instance()
		add_child(token)
