extends Projectile

const FIRE_RATE := 1.0

export var effect: PackedScene

onready var area: Area = $Area

func pulse():
	var did_hit := false
	for target in area.get_overlapping_bodies():
		if Global.is_hostile(target, team):
			did_hit = true
			target.damage(damage)

			var p := effect.instance()
			get_parent().add_child(p)
			p.global_transform.origin = global_transform.origin
			p.propagate_call("set_team", [team])
			p.set_target(target)

	if did_hit:
		emit_signal("hit")
