extends KinematicBody

#const CAMERA_ZOOM_FACTOR := 10.0
#const CAMERA_MOVE_FACTOR := 10.0
const MOUSE_SENSITIVITY := Vector2(0.05, 0.1)
const LOOK_SPEED := Vector2(1.0, 0.3)

export(Vector3) var max_velocity := Vector3(15, 9, 20)
export(Vector3) var acceleration := Vector3(30, 30, 20)
export(float) var damping := 1.0
export(NodePath) var primary_weapon
export(NodePath) var secondary_weapon

onready var anim_tree := $AnimationTree as AnimationTree

var move_target := Vector2()
var look_target := Vector2()
var look_y := 0.0
var look_x := 0.0
var velocity := Vector3()
var team := "blue"

func set_team(val: String):
	team = val
	add_to_group(val)

func _enter_tree():
	add_to_group("player")

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	propagate_call("set_team", ["blue"])

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		look_target.x += event.relative.x * MOUSE_SENSITIVITY.x
		look_target.y -= event.relative.y * MOUSE_SENSITIVITY.y

func _physics_process(delta: float):
	move_target = Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_forward") - Input.get_action_strength("move_back")
	)
	look_y -= look_target.x * LOOK_SPEED.x * delta
	look_x = clamp(look_x - look_target.y * LOOK_SPEED.y * delta, -1, 1)
	var look_offset := look_y - rotation.y
	anim_tree["parameters/move/blend_position"] = move_target.rotated(look_y)
	anim_tree["parameters/turn/add_amount"] = look_offset
	anim_tree["parameters/look_x/blend_position"] = look_x
	anim_tree["parameters/look_y/blend_position"] = fposmod(look_y, 2 * PI)

	var root_motion = anim_tree.get_root_motion_transform()
	global_transform.basis *= root_motion.basis
	look_y -= root_motion.basis.get_euler().y

	velocity = global_transform.basis.xform(root_motion.origin) / delta

	#velocity += GRAVITY * delta
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))

	#$Armature/Head.rotate(head.transform.basis.x, look_target.y * delta)
	look_target = Vector2()

	#camera.look_at(global_transform.origin + velocity * delta * CAMERA_MOVE_FACTOR, Vector3.UP)
	#camera.fov = 70 + (speed - base_speed) / base_speed * 20

	get_node(primary_weapon).active = Input.is_action_pressed("fire_primary")
