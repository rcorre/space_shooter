extends Range

signal modified

export(String, "Master", "Music", "Sound") var bus := "Master"

func _ready():
	var idx := AudioServer.get_bus_index(bus)
	value = AudioServer.get_bus_volume_db(idx)
	if connect("value_changed", self, "_set_volume", [idx]):
		push_error("connect failed")

func _set_volume(val: float, idx: int):
	AudioServer.set_bus_volume_db(idx, val)
	emit_signal("modified")
