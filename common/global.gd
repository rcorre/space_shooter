extends Node
class_name Global

const GRAVITY := Vector3(0, -9.8, 0)

enum PhysicsLayer {
	OBJECT,
	PLAYER,
	ESCORT,
	GRABBABLE,
	NPC,
	TOKEN
}

enum RenderLayer {
	DEFAULT,
	BUILD,
	BASE,
	GRAB
}

static func opposing_team(team: String) -> String:
	return "blue" if team == "red" else "red"

static func is_hostile(n: Node, to_team: String):
	return n.is_in_group(opposing_team(to_team))

static func is_friendly(n: Node, to_team: String):
	return n.is_in_group(to_team)
