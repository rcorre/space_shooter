extends GeometryInstance

export(String, "dim", "bright") var kind := "dim"

func set_team(team: String):
	var mat = load("res://assets/materials/%s/%s.tres" % [team, kind])
	material_override = mat
