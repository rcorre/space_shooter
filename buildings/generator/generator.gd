extends Spatial

export(float, 0, 1000) var MAX_HEALTH := 100.0

signal destroyed
signal respawned

var health := MAX_HEALTH
var team := "neutral"

func set_team(val: String):
	if is_in_group(team):
		remove_from_group(team)
	team = val
	add_to_group(val)
	$RespawnTimer.stop()
	respawn()

func damage(amount: float):
	health -= amount
	if health <= 0:
		destroy()

func destroy():
	$Mesh.hide()
	$RespawnTimer.start()
	$CollisionShape.disabled = true
	emit_signal("destroyed")
	Explosion.create(get_parent(), global_transform.origin, Vector3(3, 6, 3), team)

func respawn():
	health = MAX_HEALTH
	$Mesh.show()
	$CollisionShape.disabled = false
	emit_signal("respawned")
